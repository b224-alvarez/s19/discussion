// alert("Hello World!");

/* 
    SELECTION CONTROLS STRUCTURE
        - it sorts out whether the statement/s are to be executed based on the condition whether it is true or false.
        - Two-way selection (True or False)
        - Multi-way selection (if else)

*/

// If ... else Statement

/* 
    Syntax:
        if (condition) {
            //statement;
        } else {
            //statement;
        }
*/

// if statement - Executes a statement if a specified condition is true.

/* 
    // Syntax:
        if(condition){
            //statement;
        }

*/

let numA = -1;

if (numA < 0) {
  console.log("Hello");
}

console.log(numA < 0);

let city = "New York";

if (city === "New York") {
  console.log("Welcome to New York City!");
}

/* 
    Else if 
        - executes a statement if the previous conditions are false and if the specified condition is true
        - the "else if" clause is optional and can be added to capture additional conditions to change the flow of program.
*/

let numB = 1;

if (numA > 0) {
  console.log("Hello");
} else if (numB > 0) {
  console.log("World!");
}

city = "Tokyo";

if (city === "New York") {
  console.log("Welcome to New York City");
} else if (city === "Tokyo") {
  console.log("Welcome to Tokyo!");
}

/* 
    else statement
        - Executes a statement if all our conditions are false.

*/

// if (numA > 0) {
//   console.log("Hello");
// } else if (numB === 0) {
//   console.log("World");
// } else {
//   console.log("Again");
// }

// let age = parseInt(prompt("Enter your age:"));

// if (age <= 18) {
//   console.log("NOT allowed to drink");
// } else {
//   console.log("Matanda ka na, shot puno!");
// }

/*
	Mini Activty
		-create a function that will receive any value of height as an argument when you invoke it.
		-then create conditional statements:
			- if height is less than or equal to 150, print "Did not pass the min height requirement." in the console.
			- but if the height is greater than 150, print "Passed the min height requirement." in the console
*/

// function getHeight(height) {
//   if (height <= 150) {
//     console.log("Did not pass the min height requirement");
//   } else {
//     console.log("Passed the min height requirement.");
//   }
// }

// let height = parseInt(prompt("Enter your height: "));
// getHeight(height);

let message = "No message";
console.log(message);

function determineTyphoonIntensity(windSpeed) {
  if (windSpeed < 30) {
    return "Not a typhoon";
  } else if (windSpeed <= 61) {
    return "Tropical depression detected";
  } else if (windSpeed >= 62 && windSpeed <= 88) {
    return "Tropical storm detected";
  } else if (windSpeed >= 89 && windSpeed <= 177) {
    return "Severe tropical storm detected";
  } else {
    return "typhoon detected";
  }
}

message = determineTyphoonIntensity(70);
console.log(message);

console.log(determineTyphoonIntensity(90));
console.log(determineTyphoonIntensity(55));
console.log(determineTyphoonIntensity(27));
console.log(determineTyphoonIntensity(190));

if (message == "Tropical storm detected") {
  console.warn(message);
}

// Truthy and Falsy

/* 
    In JS a truthy value is a value that is considered true when encountered in a boolean context.

    Falsy value:
        1.false
        2.0
        3.-0
        4. ""
        5. null
        6. undefined
        7. NaN

*/

// Truthy Examples:

let word = "true";
if (word) {
  console.log("Truthy");
}

if (true) {
  console.log("Truthy");
}

if (1) {
  console.log("Truthy");
}

// Falsy Examples:
if (false) {
  console.log("Falsy");
}

if (0) {
  console.log("Falsy");
}

if (undefined) {
  console.log("Falsy");
}

if (null) {
  console.log("Falsy");
}

if (-0) {
  console.log("Falsy");
}

// Conditional Ternary Operator - for short codes
/* 
    Ternary Operator takes 3 operands:
        1. condition
        2. expression to execute if the condition is true/truthy
        3. expression to execute if the condition is false/falsey


    Syntax:

        (condition) ? ifTrue_expression : ifFalse_expression;
*/

// Single Statement Execution

let ternaryResult = 1 < 18 ? "Condition is True" : "Condition is False";

console.log("Result of ternary Operator:" + ternaryResult);

// Multiple Statment Execution

let name;

function isOfLegalAge() {
  name = "John";
  return "You are of the legal age limit";
}

function isUnderAge() {
  name = "Jane";
  return "You are under the age limit";
}

let yourAge = parseInt(prompt("What is your age?"));
console.log(yourAge);

let legalAge = yourAge > 18 ? isOfLegalAge() : isUnderAge();
console.log(
  "Result of Ternary Operator in Function: " + legalAge + ", " + name
);
23;

// Switch Statement

/* 
    Can be used as an alternative to an if... else statement where the data to be used in the coniditon is of an expected input.

    Syntax:
        Switch(expression) {
            case <values>:
                statement;
                break;
            default:
                statement;
        }

*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
  case "monday":
    console.log("The color of the day is red.");
    break;
  case "tuesday":
    console.log("The color of the day is orange.");
    break;
  case "wednesday":
    console.log("The color of the day is yellow.");
    break;
  case "thursday":
    console.log("The color of the day is green.");
    break;
  case "friday":
    console.log("The color of the day is blue.");
    break;
  case "saturday":
    console.log("The color of the day is indigo.");
    break;
  case "sunday":
    console.log("The color of the day is violet.");
  default:
    console.log("Please inpout a valid day");
    break;
}

// break; - is for ending the loop
// switch statment - is not a very popular statment in coding.

// Try-Catch-Finally

/* 
    - try-catch is commonly used for error handling
    - will still function even if the statement is not complete

*/
23;
function showIntensityAlert(windSpeed) {
  try {
    alert(determineTyphoonIntensity(windSpeed));
  } catch (error) {
    console.log(typeof error);
    console.log(error);
    console.warn(error.message);
  } finally {
    alert("Intensity updates will show new alert!");
  }
}

showIntensityAlert(56);
